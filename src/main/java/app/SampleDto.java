package app;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class SampleDto {
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date = LocalDate.now();
}
