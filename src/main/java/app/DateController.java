package app;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
public class DateController {


    @GetMapping("date")
    public SampleDto getDtoWithDateField() {
        return new SampleDto();
    }

    @PostMapping("date")
    public String readAndConvertDateParameter(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd")
                    LocalDate date) {

        return "Input was " + date.toString();
    }


}