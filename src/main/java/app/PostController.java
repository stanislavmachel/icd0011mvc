package app;

import model.Post;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PostController {

    private PostMemoryDao dao;

    public PostController(PostMemoryDao dao) {
        this.dao = dao;
    }

    @GetMapping("posts")
    public List<Post> getPosts() {
        return dao.findAll();
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("posts")
    public Post createPost(@RequestBody @Valid Post post) {
        return dao.save(post);
    }

    @DeleteMapping("posts/{id}")
    public void createPost(@PathVariable Long id) {
        dao.delete(id);
    }
}