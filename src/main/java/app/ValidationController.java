package app;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Post;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import validation.ValidationErrors;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import java.util.List;

@RestController
public class ValidationController {

    @PostMapping("manual-validation")
    public ResponseEntity<Object> manualValidation(@RequestBody Post post) {

        ValidationErrors errors = new ValidationErrors();

        var validator = Validation.buildDefaultValidatorFactory().getValidator();

        var violations = validator.validate(post);

        for (ConstraintViolation<Post> violation : violations) {
            errors.addErrorMessage(violation.getMessage());
        }

        if (errors.hasErrors()) {
            return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("validation")
    public void validation(@RequestBody @Valid Post post) {
        System.out.println(post);
    }

}