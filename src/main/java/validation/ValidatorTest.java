package validation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Post;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;

public class ValidatorTest {

    public static void main(String[] args) throws JsonProcessingException {
        var validator = Validation.buildDefaultValidatorFactory().getValidator();

        Post post = new Post(1L, "", null);

        var violations = validator.validate(post);

        var errors = new ValidationErrors();

        for (ConstraintViolation<Post> violation : violations) {
            errors.addErrorMessage(violation.getMessage());
            System.out.println(violation.getMessage());
        }

        System.out.println(new ObjectMapper().writeValueAsString(errors));

    }


}
